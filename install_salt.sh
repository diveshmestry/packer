sudo curl -L https://bootstrap.saltstack.com -o bootstrap_salt.sh
sudo sh bootstrap_salt.sh
if [ $? -eq 0 ]
then
        #configure salt minion by replacing /etc/salt/minion with our own minion file
        sleep 20
        #sudo mv /etc/salt/minion /etc/salt/minion.old && echo "minion backup made"
        sudo mv /srv/salt/minion /etc/salt/minion && echo "copied custom minion file"

        #restart salt-minion service
        sudo systemctl restart salt-minion

        #test salt-minion configuration
        sudo salt-call --local test.ping

        #sync custom grains
        sudo salt-call --local saltutil.sync_grains && echo "synced custom grains"

        #list custom grains
        sudo salt-call --local grains.items

        #run test state
        sudo salt-call --local state.apply test.hello

        #configure machine
        #sudo salt-call --local state.orchestrate orch.create_blog

        #no need to run as daemon
        sudo systemctl stop salt-minion
else
        echo "ERROR salt installation failed"
fi